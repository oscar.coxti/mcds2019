<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    public function __construct()
    {
        // return $this
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cat = Category::all();
        // return ($cat);
        return view('categories.index')->with('cats', $cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cat = new Category();
        $cat->name  = $request->name;
        $cat->description = $request->description;
            if ($request->hasFile('image')) {
                $file = time().'.'.$request->image->extension();
                $request->image->move(public_path('imgs'), $file);
                $cat->image = 'imgs/'.$file;
            }
        
            if ($cat->save()) {
                return redirect('categories')->with('message', 'El categoria: '.$cat->name.' Fue Adicionado con Exito!');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cat = Category::find($id);
        return view('categories.show')->with('cats', $cat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cat = Category::find($id);
        return view('categories.edit')->with('cats', $cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cat = Category::find($id);
        $cat = new Category();
        $cat->name  = $request->name;
        $cat->description     = $request->description;
            if ($request->hasFile('image')) {
                $file = time().'.'.$request->image->extension();
                $request->image->move(public_path('imgs'), $file);
                $cat->image = 'imgs/'.$file;
            }

            if ($cat->save()) {
                return redirect('categories')->with('message', 'La categoria: '.$cat->name.' Fue Modificada con Exito!');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cat = Category::find($id);
        if($cat->delete()) {
            return redirect('categories')->with('message', 'La categoria: '.$cat->fullname.' fue eliminidada con exito');
        }
    }

    public function pdf()
    {
        $cats= Category::all();
        $pdf= \PDF::loadView('categories.pdf', compact('cats'));
        return $pdf->download('categories.pdf');
    }
}
