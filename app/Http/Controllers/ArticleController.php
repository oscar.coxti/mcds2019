<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use App\Category;
use App\Exports\ArticlesExport;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $art= Article::paginate(9);
        return view('articles.index')->with('arts', $art);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $art = new Article;
        $categories=Category::all();
        $attributes = array();
        // $usr= new User;
        // $cat= new Category;
        // $usr->user_id= $request->user_id;
        // $cat->category_id= $request->category_id;
        $art->user_id = Auth::user()->id;
        $art->category_id= $request->category_id;
        $art->name  = $request->name;
        $art->description= $request->description;
        if($request->hasFile('image')){
            $file = time().'.'.$request->image->extension();
            $request->image->move(public_path('imgs'),$file);
            $art->image = 'imgs/'.$file;
        }

        if ($art->save()) {
            return redirect('articles')->with('message', 'El Artículo: '.$art->name.' Fue adicionado con éxito!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
           
        $art= Article::find($id);
        return view('articles.show')->with('art', $art);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $art = Article::find($id);
        return view('articles.edit')->with('art', $art);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $art = Article::find($id);
        $art->name  = $request->name;
        $art->description     = $request->description;
        if ($request->hasFile('image')) {
            $file = time().'.'.$request->image->extension();
            $request->image->move(public_path('imgs'), $file);
            $art->image = 'imgs/'.$file;
        }
        if ($art->save()) {
            return redirect('articles')->with('message', 'El Artículo: '.$art->name.' Fue modificado con éxito!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $art= Article::find($id);
        if($art->delete()){
            return redirect('articles')->with('message', 'El Artículo: '.$art->name.' fue eliminado con éxito');
        }
    }
    
    // public function pdf(){
    //     $arts= Article::all();
    //     $pdf= \PDF::loadView('articles.pdf', compact('arts'));
    //     return $pdf->download('articles.pdf');
    //  }
 
    //  public function excel(){
    //      return \Excel::download(new ArticlesExport, 'articles.xlsx');
         
    //  }
}
