<table>
	<thead>
		<tr>
			<th> Id </th>
			<th> Nombre Completo </th>
			<th> Descripción </th>
			<th> Foto </th>
		</tr>
    </thead>
    
    <tbody>
	@foreach($arts as $art)
		<tr>
			<td> {{ $art->id }} </td>
			<td> {{ $art->name }} </td>
			<td> {{ $art->description }} </td>
            <td><img src="{{public_path(). '/'. $art->image}}" width="10px"></td> 
			<td></td>
		</tr>
	@endforeach
	
	</tbody>
</table>
