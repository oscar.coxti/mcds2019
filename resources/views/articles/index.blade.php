@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <a href="{{ url('articles/create') }}" class="btn btn-success">
            <i class="fa fa-plus"></i> 
            Adicionar Artículo
          </a>
          <a href="{{url('generate/pdf/articles')}}" class="btn btn-dark">
          <i class="fa fa-file-pdf"></i>
              Generar PDF
          </a>
          <a href="{{url('generate/excel/articles')}}" class="btn btn-dark">
          <i class="fa fa-file-excel"></i>
              Generar Excel
          </a>
          <br><br>
          
            <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Nombre Artículo</th>
                    <th>Foto</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($arts as $art)
                    <tr>
                      <td>{{ $art->name }}</td>
                      <td><img src="{{ asset($art->image) }}" width="40px"></td>
                      <td>
                        <a href="{{ url('articles/'.$art->id) }}" class="btn btn-indigo btn-sm">
                          <i class="fa fa-search"></i>
                        </a>
                        <a href="{{ url('articles/'.$art->id.'/edit') }}" class="btn btn-indigo btn-sm">
                          <i class="fa fa-pen"></i>
                        </a>
                        <form action="{{url('articles/'.$art->id)}}" method="post" style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="button" class="btn btn-danger btn-sm btn-delete">
                        <i class="fa fa-trash"></i>
                        </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="4">
            
                    </td>
                  </tr>
                </tfoot>
            </table>
            
        </div>     
    </div>
</div>
@endsection
