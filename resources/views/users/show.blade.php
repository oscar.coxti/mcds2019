@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h1 class="h3">
              <i class="fa fa-search"></i>
              Consultar Usuario
            </h1>
            <hr>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('users')}}">Lista Usuarios</a></li>
                    <li class="breadcrumb-item active" aria-current="">Consultar Usuarios</li>
                </ol>
            </nav>
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <td colspan="2" class="text-center">
                        <img class="img-thumbnail rounded-circle" src="{{ asset($user->photo)}}" width="200px">
                    </td>
                </tr>
                <tr>
                    <th>Nombre Compelto</th>
                <td>{{ $user->fullname }}</td>
                </tr>
                <tr>
                    <th>Correo Electronico</th>
                <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>Telefono</th>
                <td>{{ $user->phone }}</td>
                </tr>
                <tr>
                    <th>Fecha de Cumpleaños</th>
                <td>
                    {{ $user->birthdate }}
                    &nbsp; | &nbsp;
                    @php use \Carbon\Carbon; @endphp
                    {{ Carbon::createFromDate($user->birthdate)->diff(Carbon::now())->format('%y años, %m meses y %d dias') }}
                </td>
                </tr>
                <tr>
                    <th>Genero</th>
                <td>
                    @if ($user->gender == "Female")
                        Mujer
                    @else
                        Hombre
                    @endif
                </td>
                </tr>
                <tr>
                    <th>Direccion Residencia</th>
                <td>{{ $user->address }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
